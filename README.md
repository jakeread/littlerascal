# Little Rascal

LR implements [these parametric linear axis](https://gitlab.cba.mit.edu/jakeread/rctgantries)

See [this important link](https://en.wikipedia.org/wiki/List_of_spacecraft_in_the_Culture_series).

This is a machine for airships, as in, airliners, as in, it is the small kit I will travel with. Fits inside of a Pelican 1650 having interior dimensions of 670x400x200mm

It has:
 - one big bed
 - one axis ~ 500mm travel
 - two axis ~ 250mm travel
 - one axis ~ 60mm travel

![img](images/littleRascal-fab-1.jpg)

Aspirationally this configuration can change, to become a larger 'dropping bed' type machine, for clay printing, or 3D printing.

Also, working on modular end effectors, and a notion of modular beds as well (i.e. a small vacuum chuck, a small vise, a small heated bed, etc)

![img](images/littleRascal-fab-3.jpg)
![img](images/littleRascal-fab-2.jpg)

## The May Update

littleRascal is pending an update, I want to
 - make motion control work with squidworks first
 - show automatic end effector swapping, or rotary-axis-turning, before:

Here's a list of things not-to-forget, for me
- beam out the beam, ferchrissake
- the bupdate (beam update) should go for a face-mounted beam a-la onsrud, etc,
 - to avoid the motor-on-face awkwardness, use the pulley mod. then can mount z back-to-face ... it would be awesome to have #globaltramming
 - then the rest of the beam is 1/8th inch alu with nut inserts, flushmount fhcs, definitely biscuits, maybe biscuits are triangular / accordioned ?
 - lightening holes tripel-eh
